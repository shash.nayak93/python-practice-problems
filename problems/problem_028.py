# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    s2=[]
    s_final = ""
    for char in s:
        if char not in s2:
            s2.append(char)

    for char in s2:
        s_final+=char
    return s_final

print(remove_duplicate_letters("hello"))
print(remove_duplicate_letters("abccbad"))
