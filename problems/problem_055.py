# Write a function that meets these requirements.
#
# Name:       simple_roman
# Parameters: one parameter that has a value from 1
#             to 10, inclusive
# Returns:    the Roman numeral equivalent of the
#             parameter value
#
# All examples
#     * input: 1
#       returns: "I"
#     * input: 2
#       returns: "II"
#     * input: 3
#       returns: "III"
#     * input: 4
#       returns: "IV"
#     * input: 5
#       returns: "V"
#     * input: 6
#       returns: "VI"
#     * input: 7
#       returns: "VII"
#     * input: 8
#       returns: "VIII"
#     * input: 9
#       returns: "IX"
#     * input: 10
#       returns:  "X"

def simple_roman():
    roman_input_str = input("input: ")
    roman_input = int(roman_input_str)


    if roman_input == 1:
        return "returns: 'I'"
    elif roman_input == 2:
        return "returns: 'II'"
    elif roman_input == 3:
        return "returns: 'III'"
    elif roman_input == 4:
        return "returns: 'IV'"
    elif roman_input == 5:
        return "returns: 'V'"
    elif roman_input == 6:
        return "returns: 'VI'"
    elif roman_input == 7:
        return "returns: 'VII'"
    elif roman_input == 8:
        return "returns: 'VIII'"
    elif roman_input == 9:
        return "returns: 'IV'"
    elif roman_input == 10:
        return "returns: 'X'"
    else:
        return ValueError

print(simple_roman())