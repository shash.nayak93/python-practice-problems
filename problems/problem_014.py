# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):

    has_flour = ingredients.count("flour")
    has_eggs = ingredients.count("eggs")
    has_oil = ingredients.count("oil")

    if has_flour>=1 and has_eggs>=1 and has_oil>=1:
        return True
    else:
        return False


print(can_make_pasta(["flour", "pineapple", "eggs", "milk", "oil"]))
print(can_make_pasta(["flour", "pineapple", "milk", "oil"]))
print(can_make_pasta(["flour", "flour", "flour", "eggs"]))
print(can_make_pasta(["flour", "flour", "flour", "eggs", "oil"]))
