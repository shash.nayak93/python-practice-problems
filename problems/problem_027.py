# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) != 0:
        max = 0
        for val in values:
            if(val>max):
                max = val
        return max
    else:
        return None

print(max_in_list([0,5,10,4,30,20]))
print(max_in_list([]))