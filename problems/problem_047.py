# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    if len(password) >= 6 and len(password)<=12:
        lower_count = 0
        upper_count = 0
        digit = 0
        special = 0
        for char in password:
            if char.isalpha() == True:
                if char.isupper()==True:
                    upper_count+=1
                else:
                    lower_count+=1
            elif char.isdigit()==True:
                digit+=1
            elif char == "$" or char =="!" or char=="@":
                special+=1
        if(lower_count >=1 and upper_count >=1 and digit >= 1 and special >=1):
            return "Okay"
        else:
            return "Not Okay"
    else:
        return "Not Okay"


print(check_password("abd123AHH$"))
print(check_password("blah57"))
