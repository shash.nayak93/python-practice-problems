# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):

    if len(values)!=0:
        print(values)
        sum = 0
        for total in values:
            sum+=total
        average = sum/len(values)
        return average
    else:
        return None

print(calculate_average([]))
print(calculate_average([20,30,25,40]))
print(calculate_average([20,10]))