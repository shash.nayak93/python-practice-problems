# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_3(number):
    if number % 3 == 0:
        return "Yes, it is divisible by 3"
    else:
        return "No, it is not divisible by 3"

print(is_divisible_by_3(6))
print(is_divisible_by_3(1232158903458906))
print(is_divisible_by_3(27))
print(is_divisible_by_3(81))
print(is_divisible_by_3(20))
