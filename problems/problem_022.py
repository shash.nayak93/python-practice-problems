# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    things = []
    if is_workday=="yes" and is_sunny=="no":
        things.append("umbrella")
    if is_workday=="yes":
        things.append("laptop")
    if is_workday=="no" and is_sunny=="yes":
        things.append("surfboard")

    return things

workday_str = input("Is it a workday? ")
workday = workday_str.lower()
sunny_str = input("Is it sunny outside? ")
sunny = sunny_str.lower()

print(gear_for_day("yes", "no"))