# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    if age >=18 and (has_consent_form=="yes" or has_consent_form=="yeah" or has_consent_form=="yessir"):
        return "You can Skydive!"
    elif age<18:
        return "Sadly, you are too young to skydive."
    elif age>=18 and (has_consent_form!="yes" and has_consent_form!="yeah" and has_consent_form!="yessir"):
        return "You must fill out the consent form to skydive."

get_age_str = input("What is your age? ")
get_age = int(get_age_str)

consent_response = input("Have you signed the consent form? ")
consent = consent_response.lower()

print(can_skydive(21, "yes"))
