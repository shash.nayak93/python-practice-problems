# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    members_attended = 0
    for members in members_list:
        for attendees in attendees_list:
            if attendees == members:
                members_attended+=1
    if members_attended >= len(members_list)/2:
        return True
    else:
        return False


print(has_quorum(["James", "Jim", "Jason", "Amy", "Sean", "Fernando", "Tien"],["Jim", "Alex", "Sally", "Tien"]))
print(has_quorum(["James", "Jim", "Jason", "Amy", "Sean", "Fernando", "Tien"],["Jim", "Alex", "Sally", "Sam"]))
print(has_quorum(["James", "Jim", "Jason", "Amy", "Sean", "Fernando", "Tien"],["Jim", "Alex", "Sally", "Tien","Johnathan","Amy","Yessi","Donna"]))
print(has_quorum(["James", "Jim", "Jason", "Amy", "Sean", "Fernando", "Tien"],["Jim", "Alex", "Sally", "Tien","Johnathan","Amy"]))
